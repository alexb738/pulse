#include "Pulse.h"
#include <PinChangeInterrupt.h>
#include <PinChangeInterruptSettings.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>

bool Pulse::Anonymous = false;

#pragma region Flags de Depuración
#define DEBUG 0 //Debug activation constant. Disable for release and for devices without serial support.
#if Receiver
#define DEBUG_LOG_PULSE false //Log every pulse (X=CLOCK, Y=DATA)
#define DEBUG_BIT_INPUT true  //Log every bit received
#define DEBUG_RAW_DATA false  //Log every received frame
#define DEBUG_RAW_STAGE false //Log the RX state
#endif
#pragma endregion

//Initializer
Pulse::Pulse(int Clock, int DataLine, int ADDRESS)
{
  _CP = Clock;
  _DP = DataLine;
  _ADD = ADDRESS;
  _STOP = BASE_SPEED;
  _HALF = _STOP / 2;
  _DEAD = _STOP * 2;
  SetReceiver();
}
Pulse::Pulse(int Clock, int DataLine)
{
  _CP = Clock;
  _DP = DataLine;
  _ADD = BROADCAST_ADDRESS;
  _STOP = BASE_SPEED;
  _HALF = _STOP / 2;
  _DEAD = _STOP * 2;
  SetReceiver();
}
//Define Physical Pins
int Pulse::_DP;      //Pin for the DATA line
int Pulse::_CP;      //Pin for the CLOCK line
int Pulse::_RxProbe; //Debug Pin (Turns HIGH when receiving data)

//Define ID Vars
int Pulse::_ADD; //System Address

//Time control definition vars
long Pulse::_DEAD; //Final Stop Time
long Pulse::_STOP; //Base Stop Time
long Pulse::_HALF; //Fast Stop Time

//Message Buffering
MessageBuffer Pulse::Buffer = MessageBuffer();
Message Pulse::M = Message();

#pragma region State Functions
void Pulse::SetTransmitter()
{
  detachPinChangeInterrupt(digitalPinToPCINT(_DP)); //Remove Data Pin Cange Handler
  detachPinChangeInterrupt(digitalPinToPCINT(_CP)); //Remove Clock Pin Cange Handler
  digitalWrite(_CP, LOW);                           //Set LOW The Clock Pin State
  digitalWrite(_DP, LOW);                           //Set LOW The Data Pin State
  pinMode(_CP, OUTPUT);                             //Configure CLOCK pin to transmit
  pinMode(_DP, OUTPUT);                             //Configure DATA pin to transmit
}
void Pulse::SetReceiver()
{
  //Write LOW on both pins to recevive
  pinMode(_CP, OUTPUT);
  pinMode(_DP, OUTPUT);
  digitalWrite(_CP, LOW);
  digitalWrite(_DP, LOW);
  //Set both pins to input
  pinMode(_CP, INPUT_PULLUP);
  pinMode(_DP, INPUT_PULLUP);
  attachPinChangeInterrupt(digitalPinToPCINT(_DP), Pulse::RXD_PULSE, RISING); //Create handler for the Data Pin Rise interrupt
  attachPinChangeInterrupt(digitalPinToPCINT(_CP), Pulse::RXC_LOW, FALLING);  //Create handler for the Clock Pin Fall interrupt
}
#pragma endregion

#pragma region Encoding Tools
/*
    Encoding functions
    Maybe, in a futer it will suport basic encryption or error detection algorythms.
*/
bool Pulse::Bit(byte _INPUT, uint8_t _POS)
//Returns the bit at the desired positon from the byte
{
  return ((_INPUT & _BITMASK[_POS]) > 0);
}
void Pulse::Boost(float OverclockFactor = 1)
//Reduces the times according with the specified factor
{
  _STOP /= OverclockFactor;
  _HALF /= OverclockFactor;
  _DEAD /= OverclockFactor;
}
void Pulse::Speed(float BaseSpeed = 200)
//Sets the new Timing accoding with the new Base Delay
{
  _STOP = BaseSpeed;
  _HALF = BaseSpeed / 2;
  _DEAD = BaseSpeed * 2;
}
#pragma endregion

#pragma region Data Transmission
#pragma(Physical Layer)
void Pulse::Mark(uint8_t Count)
//Perform INDICATOR
{
  digitalWrite(_DP, LOW);
  delayMicroseconds(_HALF);
  digitalWrite(_CP, HIGH);  //Set Clock to HIGH
  delayMicroseconds(_HALF); //Short Delay
  for (int i = 0; i < Count; i++)
  {
    //Perform as data pulses as indicated in _COUNT
    digitalWrite(_DP, HIGH);
    delayMicroseconds(_STOP); //Full Delay
    digitalWrite(_DP, LOW);
    delayMicroseconds(_HALF); //Short delay
  }
  digitalWrite(_CP, LOW);   //Set Clock to LOW
  delayMicroseconds(_HALF); //  Short Delay
}

void Pulse::ClockPulse()
//Performs a Cock Pulses
{
  delayMicroseconds(_HALF); //Short Delay
  digitalWrite(_CP, HIGH);  //Clock HIGH
  delayMicroseconds(_STOP); //Large Delay
  digitalWrite(_CP, LOW);   //Clock LOW
  delayMicroseconds(_HALF); //Short Delay
}
void Pulse::PerformBit(bool Value)
//Sets the Data Pin to the desired value and performs a clock pulse
{
  delayMicroseconds(_HALF); //Short Delay
  digitalWrite(_DP, Value); //Write Data Bit
  ClockPulse();             //Clock Pulse
  digitalWrite(_DP, LOW);   //Reset Data Pin
  delayMicroseconds(_HALF); //Short Delay
}
void Pulse::PerformByte(byte _FRAME)
// Extract the bits and send them with PerformBit()
{
  delayMicroseconds(_HALF); //
  for (byte i = 0; i < 8; i++)
  {
    PerformBit(Bit(_FRAME, i));
  }
}
#pragma endregion
#pragma region Data Transmission(Logic Layer)
#pragma region Broadcast
//Broadcast a Byte
void Pulse::BroadcastByte(byte _DATA)
{
  SendByte(BROADCAST_ADDRESS, _DATA);
}
void Pulse::BroadcastBytes(byte _DATA[], uint16_t _COUNT, uint16_t _OFFSET)
//Broadcast Bytes
{
  SendBytes(BROADCAST_ADDRESS, _DATA, _COUNT, _OFFSET);
}
#pragma endregion
#pragma region Directive Send
//Send byte to an specific device
void Pulse::SendByte(byte Dest, byte _DATA)
{
  while(_LISTENING){
    delay(0.5*_ADD);
  }
  if (!_LISTENING)
  {                           //Don't write anything if the port is being used
    delayMicroseconds(_STOP); //Long Delay
    SetTransmitter();         //Conf to transmit
    PerformByte(Dest);        //Send Dest Address
    delayMicroseconds(_STOP); //Long Delay
    PerformByte(_DATA);       //Send Data
    if (!Anonymous)
    //Tell Address?
    {
      delayMicroseconds(_STOP); //Long Delay
      Mark(3);
      delayMicroseconds(_HALF); //Short Delay
      PerformByte(_ADD);
    }
    delayMicroseconds(_STOP); //Long Delay
    Mark(2);
    SetReceiver();
  }
}
void Pulse::SendBytes(byte Dest, byte _DATA[], uint16_t _COUNT, uint16_t _OFFSET)
{
  while(_LISTENING){
    delay(0.5*_ADD);
  }
  if (!_LISTENING)
  {                           //Don't write anything if the port is being used
    delayMicroseconds(_STOP); //Long Delay
    SetTransmitter();         //Conf to transmit
    PerformByte(Dest);        //Send Dest Address
    delayMicroseconds(_STOP); //Long Delay
    for (byte i = _OFFSET; i < _OFFSET + _COUNT; i++)
    {
      //Send the specified bytes from the array
      PerformByte(_DATA[i]);
    }
    if (!Anonymous)
    //Tell Address?
    {
      delayMicroseconds(_STOP); //Long Delay
      Mark(3);
      delayMicroseconds(_HALF); //Short Delay
      PerformByte(_ADD);
    }
    delayMicroseconds(_STOP); //Long Delay
    Mark(2);                  //Terminate the Transmission
    SetReceiver();            //Reset to Listener Mode
  }
}
#pragma endregion
#pragma endregion

#pragma endregion
#pragma region Data Reception
volatile byte Pulse::_STAGE = 0;
volatile byte Pulse::i = 0;
volatile byte Pulse::c = 0;
volatile byte Pulse::B = 0x00;
volatile byte Pulse::d = 0;
volatile bool Pulse::_LISTENING = false;

void Pulse::BufferBit()
{
  bool S = digitalRead(_DP);
  B |= (S << i); //Store the read bit on the current buffer position
  i++;           //Increase current buffer position by one
}

void Pulse::RXC_LOW()
{
  if (d < 2)
  {
    BufferBit(); //Append bit on the Buffer
    if (i > 7)   //Byte Completed
    {

      i = 0; //Reset Position Counter
      if (_STAGE == ReadStages::ReceivingDest)
      {
        M.Dest = B;
        _STAGE = ReadStages::ReceivingData;
      }
      else if (_STAGE == ReadStages::ReceivingData)
      {
        M.AddByte(B);
      }
      else if (_STAGE == ReadStages::ReceivingSender)
      {
        M.From = B;
        _STAGE = ReadStages::ReceivingData;
      }
      B = 0x00; //Remove Byte from Buffer
    }
  }
  else if (d == 2)
  {
    //Reset Receiver Vars
    if (M.Dest == BROADCAST_ADDRESS || M.Dest == _ADD)
    {
      Buffer.Append(M);
    }
    _STAGE = ReadStages::ReceivingDest;
    B = 0x00;
    i = 0;
    d = 0;
    c = 0;
    M = Message();
    _LISTENING = false;
  }
  else if (d == 3)
  {
    _STAGE = ReadStages::ReceivingSender;
  }
  d = 0;
}
//End of data pulse

void Pulse::RXD_PULSE()
{
  _LISTENING = true;
  d++;
}
#pragma endregion

/*
    IF:
    2 CL pulses with DL set on HIGH
    Transmission ending. Close RX loop.
        1   _______
    DL  0 _/       \_____
        1    _   _
    CLK 0 __| |_| |______
    IF:
    3 CLK pulses with DL set on HIGH
    Receive the sender address
         1   ___________
    DL   0 _/           \_
         1    _   _   _
    CLK  0 __| |_| |_| |__
*/
/*
    The message readig stages are:
    1 Receive Header. Read the byte which makes reference to the destination.
    2 Receive Data. Read <n> bytes, the payload.
    (3) The emitter may send his addres, but is not always needed.
*/
