#include "Arduino.h"

#define MAX_MESSAGE_LENGTH 4
#define BUFFER_LENGTH 8
struct Message
{

  void AddByte(byte Data); //Add Data to the content

  byte Dest;                        //Addressee
  byte From = 0xFF;                 //Sender
  byte Content[MAX_MESSAGE_LENGTH]; //Content
  byte len = 0;                     //Content Length
};

class MessageBuffer
{
public:
  bool Available()            //Check if the buffer contains any message
  {
    return i > 0;
  }
  Message Read();           //Read the first message of the buffer
  void Append(Message Msg); //Add a message to the buffer
private:
  uint8_t i = 0;  //Last Message Index
  Message Buffer[BUFFER_LENGTH]; //Last Message Index
};
