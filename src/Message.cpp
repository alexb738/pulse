#include "Message.h"

// Append a message to the buffer
void MessageBuffer::Append(Message Msg){
    Buffer[i] = Msg;
    i++;
    if(i >= BUFFER_LENGTH) i = 0;
}

// Return the oldest message and remove from buffer
Message MessageBuffer::Read(){
    Message S = Buffer[i-1];
    for (uint8_t i = BUFFER_LENGTH - 1; i > 0; i--)
    {
        Buffer[i - 1] = Buffer[i];
    }
    i--;
    return S;
}

void Message::AddByte(byte Data){
    if(len<MAX_MESSAGE_LENGTH){
        Content[len] = Data;
        len++;
    }
}
