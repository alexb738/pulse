/*
   Class to implement pulse data protocol over digital pins
*/
#include "Arduino.h"
#include "Message.h"

class Pulse
{
#define BROADCAST_ADDRESS 0xFF
#define BASE_SPEED 50

  public:
    //Construcción
    Pulse(int CLOCK_PIN, int DATA_PIN, int ADDRESS);    //Constructor With Address
    Pulse(int CLOCK_PIN, int DATA_PIN);                 //Constructor Without Address

    void SendByte(
        byte Dest,
        byte Data);        //Send a single byte
    void SendBytes(
        byte(Dest),        //Destination
        byte _DATA[],      //Data Bytes
        uint16_t _COUNT,   //Amount
        uint16_t _OFFSET); //Offest
    void BroadcastByte(    //Broadcast a single bytes
        byte Data);        //Data
    void BroadcastBytes(   //Broadcast bytes
        byte _DATA[],      //Data Bytes
        uint16_t _COUNT,   //Amount
        uint16_t _OFFSET); //Offset

    static MessageBuffer Buffer;       //Incoming Message Buffer
    static bool Anonymous;             //Anonymous flag
    void Boost(float OverclockFactor); //Overclock function
    void Speed(float BaseDelay);    //Speed Change Function
    
    bool Available();   //The same as Available() in MessageBuffer
    bool Read();   //The same as Read() in MessageBuffer
  private:
    void SetTransmitter(); //Set Transmitter Mode
    void SetReceiver();    //Set Receiver Mode

    static long _HALF;  //Fast Stop Time
    static long _DEAD;  //Final Stop Time
    static long _STOP;  //Base Stop Time

    static int _RxProbe; //Pin for debugging purposes. It lights while receving data.

    //static Message _Message;
    static void RXD_PULSE(); //DL Interrupt handler (voltage drop)
    static void RXC_LOW();   //CLK Interrupt handler (change)
    static void BufferBit(); //Register bit

    void ClockPulse();           //Perform CLOCK Pulse
    void PerformBit(bool Value); //Send Value Bit
    void PerformByte(byte Data); //Send Data Byte
    void Mark(uint8_t Count);    //Perform Bit Indicator

    static int _DP;                  //Data line pin (DL)
    static int _CP;                  //Clock line pin (CLK/CK)
    static int _ADD;                 //Device Address

    static volatile bool _LISTENING; //Listen Flag
    volatile static byte _STAGE; //Read stage
    volatile static byte c;      //CLOCK Pulses Count
    volatile static byte d;      //DATA Pulses Coint
    volatile static byte i;      //Búffer Position
    volatile static byte B;      //RX Buffer

    static Message M;

    enum ReadStages
    {
        ReceivingDest,
        ReceivingData,
        ReceivingSender
    };

    bool Bit(byte _INPUT, uint8_t _POS); // Bit from Byte Extractor Function

    const byte _BITMASK[8] = { //Bit Masking array (to extract bits from bytes)
        0b00000001,
        0b00000010,
        0b00000100,
        0b00001000,
        0b00010000,
        0b00100000,
        0b01000000,
        0b10000000};
};
