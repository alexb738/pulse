**Before using this library...**

***Disclaimer***

This is an experimental library that comes without any waranty. 
This protocol is under development, so don't use it to control critical systems or any other application where a communication faliure may cause damage. 
This #is not# stable enough be implemented on production environment, but may be useful for research purposes or homemade projects.

**Read this**

1. You can contribute to this project, improving the implementation of this protocol.

2. You can edit and use it in your projects, and it would be great if you share any improvements.

3. If this has been useful for you, consider making a reference to this project on your program.